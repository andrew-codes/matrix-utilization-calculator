/*global define, describe, beforeEach, afterEach, it*/
'use strict';

var chai = require('chai');
var sinon = require('sinon');
chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-as-promised'));
chai.use(require('chai-datetime'));
var moment = require('moment');

describe('app/dataServices/csvTimeEntries', function () {
    var sut;
    beforeEach(function () {
        sut = require('../../../app/dataServices/csvTimeEntries');
    });

    describe('given a path to an existing CSV file', function () {
        var csvFilePath;
        beforeEach(function () {
            csvFilePath = './path/to/file';
        });
        describe('given the contents of the CSV file have correct headers', function () {
            var fs;
            beforeEach(function () {
                var csvContents = 'Duration,Start Date,Is Billable,Project,Description,Tags\n4.5,5/9/2014,Yes,Me,Test,Completed; Planned \n3.25,5/9/2014,No,MATRIX PS Internal,Test 2,Planned';
                var Fs = require('fake-fs');
                fs = new Fs();
                fs.patch();
                fs.file(csvFilePath, { content: csvContents});
            });
            afterEach(function () {
                fs.unpatch();
            });
            describe('when loading time entries from the CSV file', function () {
                var actual;
                var expected;
                var now = moment('5/9/2014');
                beforeEach(function () {
                    expected = [
                        {
                            project: 'Me',
                            description: 'Test',
                            duration: 4.5,
                            startDate: now,
                            isBillable: true,
                            tags: ['completed', 'planned'],
                            wasCompleted: true,
                            wasPlanned: true
                        },
                        {
                            project: 'MATRIX PS Internal',
                            description: 'Test 2',
                            duration: 3.25,
                            startDate: now,
                            isBillable: false,
                            tags: ['planned'],
                            wasCompleted: false,
                            wasPlanned: true
                        }
                    ];
                    actual = sut.getTimeEntriesFor(csvFilePath);
                });
                it('it should read the CSV file and parse each line as a time entry', function () {
                    return actual.should.eventually.eql(expected);
                });
            });
        });

        describe('given the contents of the CSV file has incorrect headers', function () {
            var fs;
            beforeEach(function () {
                var csvContents = 'Date,Duration\n4/9/2014,4.5';
                var Fs = require('fake-fs');
                fs = new Fs();
                fs.patch();
                fs.file(csvFilePath, { content: csvContents});
            });
            afterEach(function () {
                fs.unpatch();
            });
            it('it should throw an error stating that the headers are missing', function () {
                sut.getTimeEntriesFor(csvFilePath).should.be.rejectedWith('Missing heading column "Is Billable"');
            });
        });
    });
});