/*global define, describe, beforeEach, afterEach, it*/
'use strict';

var chai = require('chai');
var sinon = require('sinon');
chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-as-promised'));
chai.use(require('chai-datetime'));
var moment = require('moment');

describe('app/dataServices/businessDevelopmentTimeEntries', function () {
    var sut;
    beforeEach(function () {
        sut = require('../../../app/dataServices/businessDevelopmentTimeEntries');
    });

    describe('given time entries', function () {
        var timeEntries;
        beforeEach(function () {
            timeEntries = [
                {
                    project: 'MATRIX PS Internal',
                    date: moment('5/28/2014'),
                    duration: 4.5,
                    isBillable: false
                },
                {
                    project: 'MATRIX PS Internal',
                    date: moment('6/4/2014'),
                    duration: 8,
                    isBillable: false
                },
                {
                    project: 'MATRIX PS Internal',
                    date: moment('6/5/2014'),
                    duration: 6,
                    isBillable: false
                },
                {
                    project: 'MATRIX PS Internal',
                    date: moment('6/3/2014'),
                    duration: 8,
                    isBillable: false
                },
                {
                    project: 'MATRIX PS Internal',
                    date: moment('2/15/2014'),
                    duration: 7,
                    isBillable: false
                },
                {
                    project: 'MATRIX Corporate',
                    date: moment('6/3/2014'),
                    duration: 20,
                    isBillable: false
                },
                {
                    project: 'Blinds.com',
                    date: moment('6/3/2014'),
                    duration: 20,
                    isBillable: true
                }
            ];
        });
        describe('when processing time entries to determine if it is business development', function () {
            var applyTo;
            var actual;
            beforeEach(function () {
                applyTo = sinon.spy(sut, 'applyTo');
                actual = sut.applyToEach(timeEntries);
            });
            afterEach(function () {
                applyTo.restore();
            });
            it('it should process each time entry to determine if it is business development', function () {
                timeEntries.forEach(function (timeEntry) {
                    applyTo.should.be.calledWith(timeEntry);
                });
            });
            it('it should return the updated timeEntries', function () {
                actual.length.should.equal(7);
            });
        });

        describe('given a time entry for a billable project', function () {
            var timeEntry;
            beforeEach(function () {
                timeEntry = {
                    isBillable: true,
                    project: 'My Project'
                }
            });
            describe('when processing the time entry for business development', function () {
                beforeEach(function () {
                    sut.applyTo(timeEntry);
                });
                it('it should not be considered business development', function () {
                    timeEntry.isBusinessDevelopment.should.be.falsely;
                });
            });
        });

        describe('given a MATRIX Corporate project', function () {
            var timeEntry;
            beforeEach(function () {
                timeEntry = {
                    project: 'MATRIX Corporate',
                    isBillable: false
                }
            });
            describe('when processing the time entry for business development', function () {
                beforeEach(function () {
                    sut.applyTo(timeEntry);
                });
                it('it should not be considered business development', function () {
                    timeEntry.isBusinessDevelopment.should.be.falsely;
                });
            });
        });

        describe('given a time entry that is non-billable and not MATRIX Corporate', function () {
            var timeEntry;
            beforeEach(function () {
                timeEntry = {
                    project: 'MATRIX PS Internal',
                    isBillable: false
                }
            });
            describe('when processing the time entry for business development', function () {
                beforeEach(function () {
                    sut.applyTo(timeEntry);
                });
                it('it should not be considered business development', function () {
                    timeEntry.isBusinessDevelopment.should.be.truthy;
                });
            });
        });
    });
});