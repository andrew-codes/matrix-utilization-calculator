/*global define, describe, beforeEach, afterEach, it*/
'use strict';

var chai = require('chai');
var sinon = require('sinon');
chai.should();
var expect = chai.expect;
chai.use(require('sinon-chai'));
chai.use(require('chai-as-promised'));
chai.use(require('chai-datetime'));

describe('app/dataServices/toggl/togglSecurity', function () {
    var Sut,
        sut;
    beforeEach(function () {
        Sut = require('../../../../app/dataServices/toggl/togglSecurity');
    });

    describe('given a user email and Toggl API token', function () {
        var userEmail,
            togglApiToken;
        beforeEach(function () {
            userEmail = 'my@email.com';
            togglApiToken = 'hello world';
            sut = new Sut(userEmail, togglApiToken);
        });
        describe('when getting the credentials', function () {
            var actual;
            beforeEach(function () {
                actual = sut.getCredentials();
            });
            it('it should return the token encoded properly for Toggl API request header', function () {
                actual.apiToken.should.equal('aGVsbG8gd29ybGQ6YXBpX3Rva2Vu');
            });
            it('it should return the user email', function () {
                actual.userEmail.should.equal(userEmail);
            });
        });
    });
    describe('given no user email or Toggl API token have been provided', function () {
        describe('when getting the credentials', function () {
            var fn;
            beforeEach(function () {
                fn = function () {
                    sut = new Sut();
                };
            });
            it('it should throw an error', function () {
                expect(fn).to.throw('Toggl credentials have not been set');
            });
        });
    });
});