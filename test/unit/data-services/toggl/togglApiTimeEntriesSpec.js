/*global define, describe, beforeEach, afterEach, it*/
'use strict';

var chai = require('chai');
var sinon = require('sinon');
chai.should();
var expect = chai.expect;
chai.use(require('sinon-chai'));
chai.use(require('chai-as-promised'));
chai.use(require('chai-datetime'));
var moment = require('moment');
var nock = require('nock');
var querystring = require('querystring');

describe('app/dateServices/togglApiTimeEntries', function () {
    var Sut,
        sut;
    beforeEach(function () {
        Sut = require('../../../../app/dataServices/toggl/togglApiTimeEntries');
        sut = null;
    });
    describe('given no Toggl credentials', function () {
        describe('when creating a Toggl API time entries', function () {
            var fn;
            beforeEach(function () {
                fn = function () {
                    sut = new Sut();
                };
            });
            it('it should throw an error', function () {
                expect(fn).to.throw('Toggl credentials have not been set');
            });
        });
    });
    describe('given Toggl credentials', function () {
        var userEmail;
        beforeEach(function () {
            userEmail = 'mail@mail.com';
            var togglSecurityCredentials = new (require('../../../../app/dataServices/toggl/togglSecurity'))(userEmail, 'hello world');
            sut = new Sut(togglSecurityCredentials);
        });
        describe('given a workspace ID', function () {
            var workspaceId;
            beforeEach(function () {
                workspaceId = 5;
            });
            describe('when getting all of the current years time entries for the workspace from Toggl', function () {
                var actual,
                    expected;
                beforeEach(function () {
                    var page1Result = {
                        "total_grand": null,
                        "total_billable": null,
                        "total_currencies": [
                            {"currency": null, "amount": null}
                        ],
                        total_count: 3,
                        per_page: 1,
                        "data": [
                            {
                                id: 139442421,
                                pid: 4928436,
                                tid: null,
                                uid: 467204,
                                description: 'Page 1',
                                start: '2014-06-09T11:11:36-04:00',
                                end: '2014-06-09T18:05:17-04:00',
                                updated: '2014-06-09T18:12:22-04:00',
                                dur: 909000,
                                user: 'Andrew Smith',
                                use_stop: true,
                                client: 'Premier Designs',
                                project: 'Events/Awards',
                                task: null,
                                billable: 0,
                                is_billable: true,
                                cur: 'USD',
                                tags: ['Completed', 'Planned'] }
                        ]
                    };
                    var page2Result = {
                        "total_grand": null,
                        "total_billable": null,
                        "total_currencies": [
                            {"currency": null, "amount": null}
                        ],
                        total_count: 3,
                        per_page: 1,
                        "data": [
                            {
                                id: 139442421,
                                pid: 4928436,
                                tid: null,
                                uid: 467204,
                                description: 'Page 2',
                                start: '2014-06-010T11:11:36-04:00',
                                end: '2014-06-010T18:05:17-04:00',
                                updated: '2014-06-09T18:12:22-04:00',
                                dur: 909000,
                                user: 'Andrew Smith',
                                use_stop: true,
                                client: 'Premier Designs',
                                project: 'Events/Awards',
                                task: null,
                                billable: 0,
                                is_billable: true,
                                cur: 'USD',
                                tags: [] }
                        ]
                    };
                    var page3Result = {
                        "total_grand": null,
                        "total_billable": null,
                        "total_currencies": [
                            {"currency": null, "amount": null}
                        ],
                        total_count: 3,
                        per_page: 1,
                        "data": [
                            {
                                id: 139442421,
                                pid: 4928436,
                                tid: null,
                                uid: 467204,
                                description: 'Page 3',
                                start: '2014-06-011T11:11:36-04:00',
                                end: '2014-06-011T18:05:17-04:00',
                                updated: '2014-06-09T18:12:22-04:00',
                                dur: 909000,
                                user: 'Andrew Smith',
                                use_stop: true,
                                client: 'Premier Designs',
                                project: 'Events/Awards',
                                task: null,
                                billable: 0,
                                is_billable: true,
                                cur: 'USD',
                                tags: ['Completed'] }
                        ]
                    };
                    expected = [
                        {
                            project: 'Events/Awards',
                            description: 'Page 1',
                            duration: 0.25,
                            isBillable: true,
                            startDate: moment('2014-06-09T11:11:36-04:00').utc().startOf('day'),
                            tags: ['completed', 'planned'],
                            wasCompleted: true,
                            wasPlanned: true
                        },
                        {
                            project: 'Events/Awards',
                            description: 'Page 2',
                            duration: 0.25,
                            isBillable: true,
                            startDate: moment('2014-06-010T11:11:36-04:00').utc().startOf('day'),
                            tags: [],
                            wasCompleted: false,
                            wasPlanned: false
                        },
                        {
                            project: 'Events/Awards',
                            description: 'Page 3',
                            duration: 0.25,
                            isBillable: true,
                            startDate: moment('2014-06-011T11:11:36-04:00').utc().startOf('day'),
                            tags: ['completed'],
                            wasCompleted: true,
                            wasPlanned: false
                        }
                    ];
                    var queryString = querystring.stringify({
                        workspace_id: workspaceId,
                        since: moment().startOf('year').format('YYYY-MM-DD'),
                        page: 1,
                        user_agent: userEmail
                    });
                    var queryString2 = querystring.stringify({
                        workspace_id: workspaceId,
                        since: moment().startOf('year').format('YYYY-MM-DD'),
                        page: 2,
                        user_agent: userEmail
                    });
                    var queryString3 = querystring.stringify({
                        workspace_id: workspaceId,
                        since: moment().startOf('year').format('YYYY-MM-DD'),
                        page: 3,
                        user_agent: userEmail
                    });
                    nock('https://toggl.com')
                        .get('/reports/api/v2/details?' + queryString)
                        .reply(200, page1Result)
                        .get('/reports/api/v2/details?' + queryString2)
                        .reply(200, page2Result)
                        .get('/reports/api/v2/details?' + queryString3)
                        .reply(200, page3Result);

                    actual = sut.getCurrentYearTimeEntriesFor(workspaceId);
                });
                it('it should return a promise with the all time entries for the current year', function () {
                    return  actual.should.eventually.eql(expected);
                });
            });
        });
    });
})
;