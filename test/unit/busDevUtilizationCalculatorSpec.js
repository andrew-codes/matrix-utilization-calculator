/*global define, describe, beforeEach, afterEach, it*/
'use strict';

var chai = require('chai');
var sinon = require('sinon');
chai.should();
chai.use(require('sinon-chai'));
chai.use(require('chai-as-promised'));
chai.use(require('chai-datetime'));
var moment = require('moment');

describe('app/busDevUtilizationCalculator', function () {
    var sut;
    beforeEach(function () {
        sut = require('../../app/busDevUtilizationCalculator');
    });

    describe('given a collection of time entries', function () {
        var timeEntries,
            now;
        beforeEach(function () {
            now = moment('06/06/2014');
            timeEntries = [
                {
                    startDate: moment('5/28/2014'),
                    duration: 4.5,
                    isBillable: false,
                    isBusinessDevelopment: true
                },
                {
                    startDate: moment('6/4/2014'),
                    duration: 8,
                    isBillable: false,
                    isBusinessDevelopment: true
                },
                {
                    startDate: moment('6/5/2014'),
                    duration: 6,
                    isBillable: false,
                    isBusinessDevelopment: true
                },
                {
                    startDate: moment('6/3/2014'),
                    duration: 8,
                    isBillable: false,
                    isBusinessDevelopment: true
                },
                {
                    startDate: moment('2/15/2014'),
                    duration: 7,
                    isBillable: false,
                    isBusinessDevelopment: true
                }
            ];
        });
        describe('when calculating weekly utilization', function () {
            var actual,
                expected;
            beforeEach(function () {
                expected = 55;
                actual = sut.weekly(now, timeEntries);
            });
            it('it should calculate utilization for week of the provided date for only time entries that are billable', function () {
                actual.should.equal(expected);
            });
        });

        describe('given a date', function () {
            var now;
            beforeEach(function () {
                now = moment('6/6/2014');
            });

            describe('when calculating quarterly utilization for the current quarter of the provided date', function () {
                var actual,
                    expected;
                beforeEach(function () {
                    expected = 7.36;
                    actual = sut.quarterly(now, timeEntries);
                });
                it('it should calculate utilization for the quarter of the provided date to current date for only time entries that are billable', function () {
                    actual.should.be.equal(expected);
                });
            });

            describe('when calculating the yearly utilization for the year of the provided date to current date', function () {
                var actual,
                    expected;
                beforeEach(function () {
                    expected = 3.81;
                    actual = sut.yearly(now, timeEntries);
                });
                it('it should calculate utilization for the year of the provided date to current date for only time entries that are billable', function () {
                    actual.should.be.equal(expected);
                });
            });
        });
    });
});