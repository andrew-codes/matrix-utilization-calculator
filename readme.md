The MATRIX utilization calculator is used to calculate a developer's utilization for the week, quarter to date, and year to date. These calculations are based on time entries from a CSV file or Toggl via Toggl's API.

# Installation

Clone the repository and change directory to the root directory.

```bash
npm link
```

# Usage

*It is assumed that your data source has all the time entries for the current year; otherwise it cannot calculate the quarterly and yearly utilization properly*

You will need to copy the `example.config.json` to `config.json` and fill in with your details. See each section below for their respective configuration details.

## CSV Datasource

Enter the path to your CSV file in your `config.json` file. Remember the CSV file **must conform** to the following requirements:

1. The first line must be the headers
2. Must include at least the following headers:
  * Duration
  * Start Date
  * Project
  * Description
  * Is Billable
  * Tags (a semi-colon delimited list of tags)
3. Headers may be any in order

Once you have a CSV file, run the following command; specifying a date falling within the week you are targeting to calculate your utilization. *If no `weekOf` is specified, it will default to the previous week from the current date.*

```bash
matrix-utilization --csv --weekOf 'a date string'
```

## Toggl API Datasource

To integrate with Toggl, you will need to enter with your Toggl email address and Toggl API key into the `config.json`. *Your API key can be found on the toggle website under your profile.*

After filling the configuration file with your information, run the following command; specifying a date falling within the week you are targeting to calculate your utilization. *If no `weekOf` is specified, it will default to the previous week from the current date.*

```bash
matrix-utilization --toggl --weekOf 'a date string'
```

# Tests

You will be able to run the tests with the following commands:

```bash
npm install
npm test
```
