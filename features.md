# User Stories

## UC-1
As a developer, I want to be able to pull my time entries from a CSV file, so that I can calculate my utilizations.

* Given a CSV file with correct headers
  * import time entries
* Given a CSV file without headers/correct headers
  * notify user that the following headers are required
    * Date
    * Duration
    * Is Billable

## UC-2
As a developer, I want to be able to calculate my utilization for my billable time entries, so that I can include these values on my weekly status report.

Utilization is considered the percentage value from the number of worked hours of the total number of hours; for a given time period of week, quarter, year. The quarter and year calculations are assuming a running total number of hours based on the previous week and not a static number.

* Given time entires
  * calculate previous week's utilization for billable time
  * calculate current quarter to end of previous week's utilization for billable time
  * calculate current year to end of previous week's utilization for billable time
* Given no billable time entries
  * calculate utilization for week, quarter, year as 0%

## UC-3
As a developer, I want to be able to calculate my non-billable utilization, so that I can include these values on my weekly status report.

Utilization is considered the percentage value from the number of worked hours of the total number of hours; for a given time period of week, quarter, year. The quarter and year calculations are assuming a running total number of hours based on the previous week and not a static number.

* Given time entires
  * calculate previous week's utilization for non-billable time
  * calculate current quarter to end of previous week's utilization for non-billable time
  * calculate current year to end of previous week's utilization for non-billable time
* Given no non-billable time entries
  * calculate utilization for week, quarter, year as 0%

## UC-4
As a developer, I want to be able to pull my time entries from Toggl, so that I can calculate my utilizations from my time entries entered via Toggl.

* Given a Toggl API key
  * connect to Toggl
  * download all time entries for current year
* Given a failure to connect to Toggl
  * notify the user of the failure and why it occurred
