#! /usr/bin/env node
'use strict';

var q = require('q');
var args = require('yargs').argv;
var moment = require('moment');
require('moment-range');
var isCsvMode = args.csv;
var isTogglMode = args.toggl;
var csvTimeEntries = require('./dataServices/csvTimeEntries');
var TogglTimeEntries = require('./dataServices/toggl/togglApiTimeEntries');
var TogglSecurity = require('./dataServices/toggl/togglSecurity');
var billableUtilizationCalculator = require('./billableUtilizationCalculator');
var busDevUtilizationCalculator = require('./busDevUtilizationCalculator');
var businessDevelopment = require('./dataServices/businessDevelopmentTimeEntries');
var config = require('./../config.json');
var DocxTemplater = require('docxtemplater');
var fs = require('fs');

var weekOf = null;
if (args.weekOf != null) {
    weekOf = moment(args.weekOf);
}
else {
    weekOf = moment().subtract('week', 1);
}
weekOf = weekOf.utc().startOf('week');

var timeEntries;
if (isCsvMode) {
    timeEntries = csvTimeEntries.getTimeEntriesFor(config.csv.filePath);
}
else if (isTogglMode) {
    var togglSecurity = new TogglSecurity(config.toggl.userEmail, config.toggl.apiToken);
    var togglTimeEntries = new TogglTimeEntries(togglSecurity);
    timeEntries = togglTimeEntries.getCurrentYearTimeEntriesFor(554110);
}
else {
    throw 'No mode specified: --csv and --toggl are supported';
}
q.when(timeEntries)
    .then(businessDevelopment.applyToEach)
    .then(function (timeEntries) {
        var endOfWeek = moment(weekOf).utc().endOf('week');
        var fileName = endOfWeek.format('YYYY-MM-DD') + ' ' + config.name + ' Weekly Status.docx';
        if (fs.existsSync(fileName)) {
            fs.unlinkSync(fileName);
        }
        var docx = new DocxTemplater().loadFromFile("weekly-status-template.docx"),
            completedPlanned = [],
            completedUnplanned = [],
            incompletePlanned = [];
        var range = moment().range(weekOf, endOfWeek);
        timeEntries.filter(function (timeEntry) {
            return timeEntry.startDate.within(range);
        }).
            forEach(function (timeEntry) {
                if (timeEntry.wasCompleted && timeEntry.wasPlanned) {
                    completedPlanned.push({description: timeEntry.description});
                }
                else if (timeEntry.wasCompleted) {
                    completedUnplanned.push({description: timeEntry.description});
                }
                else if (timeEntry.wasPlanned) {
                    incompletePlanned.push({description: timeEntry.description});
                }
            });
        var docTags = {
            name: config.name,
            weekOf: endOfWeek.format('YYYY-MM-DD'),
            completedPlanned: unique(completedPlanned),
            completedUnplanned: unique(completedUnplanned),
            incompletePlanned: unique(incompletePlanned),
            weeklyUtilization: billableUtilizationCalculator.weekly(weekOf, timeEntries),
            quarterlyUtilization: billableUtilizationCalculator.quarterly(weekOf, timeEntries),
            yearlyUtilization: billableUtilizationCalculator.yearly(weekOf, timeEntries),
            weeklyBusDevUtilization: busDevUtilizationCalculator.weekly(weekOf, timeEntries),
            quarterlyBusDevUtilization: busDevUtilizationCalculator.quarterly(weekOf, timeEntries),
            yearlyBusDevUtilization: busDevUtilizationCalculator.yearly(weekOf, timeEntries)
        };
        docx.setTags(docTags);
        docx.finishedCallback = function () {
            docx.output(
                {
                    name: fileName
                });
        };
        docx.applyTags();
    });

function unique(collection) {
    var objArray = {},
        index,
        length = collection.length,
        output = [];
    for (index = 0; index < length; index += 1) {
        objArray[collection[index]] = collection[index];
    }
    Object.keys(objArray).forEach(function(key){
        output.push(objArray[key]);
    });
    return output;
}
