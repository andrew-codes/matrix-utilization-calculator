'use strict';
var utilizationCalculator = require('./utilizationCalculator');

var busDevUtilizationCalculator = {
    weekly: weekly,
    quarterly: quarterly,
    yearly: yearly
};

function weekly(date, timeEntries) {
    var relevantTimeEntires = timeEntries.filter(function (timeEntry) {
        return timeEntry.isBusinessDevelopment;
    });
    return utilizationCalculator.weekly(date, relevantTimeEntires);
}

function quarterly(date, timeEntries) {
    var relevantTimeEntires = timeEntries.filter(function (timeEntry) {
        return timeEntry.isBusinessDevelopment;
    });
    return utilizationCalculator.quarterly(date, relevantTimeEntires);
}

function yearly(date, timeEntries) {
    var relevantTimeEntires = timeEntries.filter(function (timeEntry) {
        return timeEntry.isBusinessDevelopment;
    });
    return   utilizationCalculator.yearly(date, relevantTimeEntires);
}

module.exports = busDevUtilizationCalculator;