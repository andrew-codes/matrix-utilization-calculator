'use strict';
var utilizationCalculator = require('./utilizationCalculator');

var billableUtilizationCalculator = {
    weekly: weekly,
    quarterly: quarterly,
    yearly: yearly
};

function weekly(date, timeEntries) {
    var relevantTimeEntries = timeEntries.filter(function (timeEntry) {
        return timeEntry.isBillable;
    });
    return  utilizationCalculator.weekly(date, relevantTimeEntries);
}

function quarterly(date, timeEntries) {
    var relevantTimeEntries = timeEntries.filter(function (timeEntry) {
        return timeEntry.isBillable;
    });
    return utilizationCalculator.quarterly(date, relevantTimeEntries);
}

function yearly(date, timeEntries) {
    var relevantTimeEntries = timeEntries.filter(function (timeEntry) {
        return timeEntry.isBillable;
    });
    return   utilizationCalculator.yearly(date, relevantTimeEntries);
}

module.exports = billableUtilizationCalculator;