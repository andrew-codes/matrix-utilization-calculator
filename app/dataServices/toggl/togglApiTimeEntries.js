'use strict';

var https = require('https');
var extend = require('extend');
var q = require('q');
var querystring = require('querystring');
var moment = require('moment');

module.exports = function (togglSecurityCredentials) {
    var self = this;
    self.getCurrentYearTimeEntriesFor = getCurrentYearTimeEntriesFor;
    self.getTimeEntriesFor = getTimeEntriesFor;
    init(togglSecurityCredentials);

    function init(securityCredentials) {
        if (securityCredentials == null) {
            throw 'Toggl credentials have not been set';
        }
    }

    function getCurrentYearTimeEntriesFor(workspaceId) {
        return getTimeEntriesFor(workspaceId, moment().startOf('year'))
    }

    function getTimeEntriesFor(workspaceId, sinceDate) {
        var criteria = {
            workspace_id: workspaceId,
            since: sinceDate.format('YYYY-MM-DD'),
            page: 1
        };
        var path = '/reports/api/v2/details';
        var deferredTimeEntries = q.defer();
        fetchApiData(path, criteria).then(function (results) {
                var timeEntries = processTogglTimeEntryData(results);
                var additionalRequests = fetchAdditionalPagesFor(path, results, criteria);
                q.all(additionalRequests)
                    .spread(function () {
                        for (var argumentIndex = 0; argumentIndex < arguments.length; argumentIndex++) {
                            timeEntries = timeEntries.concat(processTogglTimeEntryData(arguments[argumentIndex]));
                        }
                    })
                    .done(function () {
                        deferredTimeEntries.resolve(timeEntries);
                    });
            },
            function (error) {
                deferredTimeEntries.reject(error);
            });
        return deferredTimeEntries.promise;
    }

    function fetchAdditionalPagesFor(path, results, criteria) {
        var totalPages = Math.round(results.total_count / results.per_page);
        var additionalRequests = [];
        for (var i = 2; i <= totalPages; i++) {
            criteria.page = i;
            additionalRequests.push(fetchApiData(path, criteria));
        }
        return additionalRequests;
    }

    function fetchApiData(path, queryCriteria) {
        var options = getRequestOptions(path, queryCriteria);
        var deferredResults = q.defer();
        https.get(options, function (res) {
            var data = '';
            res.on('data', function (d) {
                data += d;
            });
            res.on('end', function () {
                var results = JSON.parse(data.toString());
                deferredResults.resolve(results);
            });
            res.on('error', function (error) {
                deferredResults.reject(error);
            });
        }).on('error', function (error) {
            deferredResults.reject(error);
        });
        return deferredResults.promise;
    }

    function getRequestOptions(path, queryCriteria) {
        var credentials = togglSecurityCredentials.getCredentials();
        var options = {
            host: 'toggl.com',
            port: 443,
            method: 'GET',
            accept: 'application/json',
            headers: {
                'Content-type': 'application/json',
                Authorization: 'Basic ' + credentials.apiToken
            }
        };
        queryCriteria.user_agent = credentials.userEmail;
        options.path = path + '?' + querystring.stringify(queryCriteria);
        return options;
    }

    function processTogglTimeEntryData(resultTimeEntries) {
        var timeEntriesOutput = [];
        resultTimeEntries.data.forEach(function (timeEntry) {
            timeEntriesOutput.push(parseTimeEntry(timeEntry));
        });
        return timeEntriesOutput;
    }

    function parseTimeEntry(timeEntry) {
        var entry = {
            project: timeEntry.project,
            duration: Math.round(timeEntry.dur / 36000) / 100,
            startDate: moment(timeEntry.start).utc().startOf('day'),
            isBillable: timeEntry.is_billable,
            description: timeEntry.description,
            tags: parseTags(timeEntry.tags)
        };
        entry.wasCompleted = entry.tags.indexOf('completed') > -1;
        entry.wasPlanned = entry.tags.indexOf('planned') > -1;
        return entry;
    }

    function parseTags(tags) {
        for (var index = 0; index < tags.length; index++) {
            tags[index] = tags[index].toLowerCase();
        }
        return tags;
    }
};