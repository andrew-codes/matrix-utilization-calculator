'use strict';
var encodedToken;
var userEmailAddress;

module.exports = function (userEmail, apiToken) {
    var self = this;
    var encodedToken;
    var userEmailAddress;
    self.getCredentials = getCredentials;
    init(userEmail, apiToken);

    function init(userEmail, apiToken) {
        if (userEmail == null || apiToken == null) {
            throw 'Toggl credentials have not been set';
        }
        userEmailAddress = userEmail;
        encodedToken = new Buffer(apiToken + ':api_token').toString('base64');
    }

    function getCredentials() {
        return {
            apiToken: encodedToken,
            userEmail: userEmailAddress
        }
    }
};