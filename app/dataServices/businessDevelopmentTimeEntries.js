'use strict';

var businessDevelopmentTimeEntries = {
    applyToEach: applyToEach,
    applyTo: applyTo
};

function applyToEach(timeEntries) {
    timeEntries.forEach(function (timeEntry) {
        businessDevelopmentTimeEntries.applyTo(timeEntry);
    });
    return timeEntries;
}

function applyTo(timeEntry) {
    timeEntry.isBusinessDevelopment = (!timeEntry.isBillable && timeEntry.project.toLowerCase() !== 'matrix corporate');
}

module.exports = businessDevelopmentTimeEntries;