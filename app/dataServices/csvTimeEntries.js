'use strict';

var q = require('q');
var fs = require('fs');
var moment = require('moment');

var columnHeadingKeys = [
    'Project',
    'Start Date',
    'Duration',
    'Is Billable',
    'Description',
    'Tags'
];

module.exports = {
    getTimeEntriesFor: loadTimeEntriesFrom
};

function loadTimeEntriesFrom(csvFilePath) {
    var readFile = q.denodeify(fs.readFile);
    return readFile(csvFilePath, 'utf8')
        .then(parseTimeEntries);
}

function parseTimeEntries(csvContents) {
    var timeEntriesOutput = [];
    var entries = csvContents.split('\n');
    var headings = parseColumnHeadings(entries[0]);
    validateColumnHeadings(headings);
    entries.slice(1).forEach(function (timeEntryString) {
        timeEntriesOutput.push(parseTimeEntry(timeEntryString, headings));
    });
    return timeEntriesOutput;
}

function parseColumnHeadings(headingString) {
    var columns = headingString.split(',');
    var headings = {};
    columns.forEach(function (heading) {
        columnHeadingKeys.forEach(function (columnHeadingKey) {
            var headingKey = heading.toLowerCase();
            if (headingKey === columnHeadingKey.toLowerCase()) {
                headings[columnHeadingKey] = columns.indexOf(heading);
            }
        });
    });
    return headings;
}

function validateColumnHeadings(headings) {
    columnHeadingKeys.forEach(function (key) {
        if (headings[key] == undefined) {
            throw 'Missing heading column "' + key + '"';
        }
    });
}

function parseTimeEntry(timeEntryString, columnHeadingKeys) {
    var columns = timeEntryString.split(',');
    var duration = parseFloat(columns[columnHeadingKeys['Duration']]) || 0;
    var timeEntry = {
        project: columns[columnHeadingKeys['Project']],
        description: columns[columnHeadingKeys['Description']],
        duration: duration,
        startDate: moment(columns[columnHeadingKeys['Start Date']]),
        isBillable: columns[columnHeadingKeys['Is Billable']].toLowerCase() === 'yes',
        tags: parseTags(columns[columnHeadingKeys['Tags']])
    };
    timeEntry.wasCompleted = timeEntry.tags.indexOf('completed') > -1;
    timeEntry.wasPlanned = timeEntry.tags.indexOf('planned') > -1;
    return timeEntry;
}

function parseTags(tagsString) {
    var tags = tagsString.toLowerCase().split(';');
    for(var index = 0; index < tags.length; index++){
        tags[index] = tags[index].trim();
    }
    return tags;
}

