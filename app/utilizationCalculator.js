'use strict';

var moment = require('moment');

var utilizationCalculator = {
    weekly: weekly,
    quarterly: quarterly,
    yearly: yearly
};

function weekly(date, timeEntries) {
    var week = date.week();
    var relevantTimeEntries = timeEntries.filter(function (timeEntry) {
        return timeEntry.startDate.week() === week;
    });
    return calculateFor(40, relevantTimeEntries);
}

function quarterly(date, timeEntries) {
    var quarter = date.quarter();
    var relevantTimeEntries = timeEntries.filter(function (timeEntry) {
        return timeEntry.startDate.quarter() === quarter;
    });
    var currentQuarter = moment().startOf('quarter');
    var weeksInQuarterToNow = date.diff(currentQuarter, 'weeks');
    var totalTime = weeksInQuarterToNow * 40;
    return calculateFor(totalTime, relevantTimeEntries);
}

function yearly(date, timeEntries) {
    var year = date.year();
    var relevantTimeEntries = timeEntries.filter(function (timeEntry) {
        return timeEntry.startDate.year() === year;
    });
    var daysInYearFromDate = date.diff(moment().startOf('year'), 'weeks');
    var totalTime = daysInYearFromDate * 40;
    return calculateFor(totalTime, relevantTimeEntries);
}

function calculateFor(totalDuration, timeEntries) {
    var runningTotal = 0;
    timeEntries.forEach(function (timeEntry) {
        runningTotal += timeEntry.duration;
    });
    var utilization = totalDuration === 0 ? 0 : (runningTotal / totalDuration);
    return (Math.round(utilization * 10000)) / 100;
}

module.exports = utilizationCalculator;